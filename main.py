# Блок импортов
import sys
import os
# Импорт модуля времени для подсчета времени выполнения операций.
import time
# Импорт файла с интерфейсом пользователя для взаимодействия с его объектами
import design
# Импорт библиотеки numpy для тестирования решения поставленных задач с ее помощью
import numpy as np
# Импорт библиотеки tensorflow для решения главной задачи
import tensorflow as tf
# Импорт библиотеки matlab для тестирования решения поставленных задач с ее помощью
import matlab as ml
# Импорт модулей PyQT для реализации взаимодействия с интерфейсом.
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem, QPlainTextEdit, QMessageBox
# Условие, необходимое для корректной работы с tensorFlow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Класс взаимодействия с главным окном программы
class DiplomApp(QtWidgets.QMainWindow, design.Ui_mainWindow):

    # Блок глобальных переменных, необходимых для подготовки данных и передачи их функциям решения поставленных задач
    mulLeftMatrix = [] # Левая матрица для операции умножения
    mulRightMatrix = [] # Правая матрица для операции умножения
    slauCoefMatrix = [] # Матрица коэффициентов Х для решения системы линейных алгебраических уравнений
    slauAnswMatrix = [] # Матрица ответов на каждое уравнение системы линейных алгебраических уравнений

    # Функция инициализации окна.
    # Подключение сигналов к слотам
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.leftMatrixPath.clicked.connect(self.fillLeftMatrix)
        self.rightMatrixPath.clicked.connect(self.fillRightMatrix)
        self.ChangeMatrixSize.clicked.connect(self.setMatrixSize)
        self.leftMatrixCol.valueChanged.connect(self.changeRightMatrixRow)
        self.multiplyMatrix.clicked.connect(self.multiplyMatrixResult)
        self.slauAccept.clicked.connect(self.slauMatrixNew)
        self.solveSLAU.clicked.connect(self.solve)
        self.coefMatrixPath.clicked.connect(self.fillCoefMatrix)
        self.answMatrixPath.clicked.connect(self.fillAnswMatrix)
        self.generate.clicked.connect(self.generateMatrix)
        self.mulNumPy.clicked.connect(self.multiplyNumpy)
        self. mulMatLab.clicked.connect(self.multiplyMatLab)
        self.solveNumPy.clicked.connect(self.slauNumPy)
        self.solveMatLab.clicked.connect(self.slauMatLab)

    # Слот для изменения размеров правой матрицы при умножении
    def changeRightMatrixRow(self):
        self.rightMatrixRow.setValue(self.leftMatrixCol.value())

    # Заполнение левой матрицы при вводе из файла
    def fillLeftMatrix(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "/home", "*.txt")[0]
        self.mulLeftMatrix = []
        if fname != "":
            with open(fname, 'r') as f:
                for line in f:
                    try:
                        self.mulLeftMatrix.append([float(i) for i in line.split()])
                    except ValueError:
                        QMessageBox.warning(self, "Warning", "Поданные на вход данные не соответствуют требованиям\n"
                                                             "Проверьте, возможно в файле использованы скобки, запятые, либо\n"
                                                             " числа переданы неверно. Float можно передавать в форматах -\n"
                                                             "  - через точку. 0.154 0.1222\n"
                                                             "  - через степень. 1e-5 2e10")
                        return
            for i in range(1, len(self.mulLeftMatrix)):
                if len(self.mulLeftMatrix[i]) != len(self.mulLeftMatrix[i - 1]):
                    QMessageBox.warning(self, "Warning", "Матрица имеет строки разной длины.")
                    return
            self.setLeftMatrixData()

    # Установка поведения виджетов главного окна для левой матрицы
    def setLeftMatrixData(self):
        self.leftMatrix.clear()
        self.leftMatrixRow.setValue(len(self.mulLeftMatrix))
        self.leftMatrixCol.setValue(len(self.mulLeftMatrix[0]))
        self.leftMatrix.setRowCount(len(self.mulLeftMatrix))
        self.leftMatrix.setColumnCount(len(self.mulLeftMatrix[0]))
        for i in range(len(self.mulLeftMatrix)):
            for j in range(len(self.mulLeftMatrix[i])):
                self.leftMatrix.setItem(i, j, QTableWidgetItem(str(self.mulLeftMatrix[i][j])))

    # Установка поведения виджетов главного окна для правой матрицы
    def setRightMatrixData(self):
        self.rightMatrix.clear()
        self.rightMatrixRow.setValue(len(self.mulRightMatrix))
        self.rightMatrixCol.setValue(len(self.mulRightMatrix[0]))
        self.rightMatrix.setRowCount(len(self.mulRightMatrix))
        self.rightMatrix.setColumnCount(len(self.mulRightMatrix[0]))
        for i in range(len(self.mulRightMatrix)):
            for j in range(len(self.mulRightMatrix[i])):
                self.rightMatrix.setItem(i, j, QTableWidgetItem(str(self.mulRightMatrix[i][j])))

    # Заполнение правой матрицы при вводе из файла
    def fillRightMatrix(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "/home", "*.txt")[0]
        self.mulRightMatrix = []
        if fname != "":
            with open(fname, 'r') as f:
                for line in f:
                    try:
                        self.mulRightMatrix.append([float(i) for i in line.split()])
                    except ValueError:
                        QMessageBox.warning(self, "Warning", "Поданные на вход данные не соответствуют требованиям\n"
                                                             "Проверьте, возможно в файле использованы скобки, запятые, либо\n"
                                                             " числа переданы неверно. Float можно передавать в форматах -\n"
                                                             "  - через точку. 0.154 0.1222\n"
                                                             "  - через степень. 1e-5 2e10")
                        return
            for i in range(1, len(self.mulRightMatrix)):
                if len(self.mulRightMatrix[i]) != len(self.mulRightMatrix[i - 1]):
                    QMessageBox.warning(self, "Warning", "Матрица имеет строки разной длины.")
                    return
            self.setRightMatrixData()

    # Установка размеров матриц при нажатии кнопки ввода данных от пользователя
    def setMatrixSize(self):
        self.leftMatrix.clear()
        self.rightMatrix.clear()
        self.leftMatrix.setRowCount(self.leftMatrixRow.value())
        self.leftMatrix.setColumnCount(self.leftMatrixCol.value())
        self.rightMatrix.setRowCount(self.rightMatrixRow.value())
        self.rightMatrix.setColumnCount(self.rightMatrixCol.value())

        for i in range(self.leftMatrix.rowCount()):
            for j in range(self.leftMatrix.columnCount()):
                self.leftMatrix.setItem(i, j, QTableWidgetItem(str('')))

        for i in range(self.rightMatrix.rowCount()):
            for j in range(self.rightMatrix.columnCount()):
                self.rightMatrix.setItem(i, j, QTableWidgetItem(str('')))

    # Умножение матриц с помощью ТензорФлоу
    def multiplyMatrixResult(self):

        var = time.time()
        # Проверка, чтоб в матрицах хоть что-то было
        if self.leftMatrix.columnCount() == 0 or self.leftMatrix.rowCount() == 0 or self.rightMatrix.columnCount() == 0 \
                or self.rightMatrix.rowCount() == 0 or self.leftMatrix.columnCount() != self.rightMatrix.rowCount():
            QMessageBox.warning(self, "Warning", "Количесво строк и столбцов в матрицах не позволяет их перемножить")
            return

        self.mulLeftMatrix = []
        self.mulRightMatrix = []

        # Подготовка данных для функции
        for i in range(self.leftMatrix.rowCount()):
            self.mulLeftMatrix.append([])
            for j in range(self.leftMatrix.columnCount()):
                try:
                    float(self.leftMatrix.item(i, j).text())
                    self.mulLeftMatrix[i].append(float(self.leftMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в левой матрице невозможно представить в виде float.")
                    self.leftMatrix.setCurrentCell(i, j)
                    return
        npLeftMul = np.asarray(self.mulLeftMatrix, dtype=np.float32)

        for i in range(self.rightMatrix.rowCount()):
            self.mulRightMatrix.append([])
            for j in range(self.rightMatrix.columnCount()):
                try:
                    float(self.rightMatrix.item(i, j).text())
                    self.mulRightMatrix[i].append(float(self.rightMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в правой матрице невозможно представить в виде float.")
                    self.rightMatrix.setCurrentCell(i, j)
                    return
        npRightMul = np.asarray(self.mulRightMatrix, dtype=np.float32)

        # Работа с Функцией
        try:
            result = tf.linalg.matmul(npLeftMul, npRightMul)
            sess = tf.Session()
            text = sess.run(result)
            sess.close()
        except ValueError:
            QMessageBox.warning(self, "Решение", "Не удалось перемножить матрицы.")
            return

        var2 = time.time()
        my_file = open('MultiplyResult.txt', 'w')
        for i in range(len(text)):
            my_file.write(str(text[i]) + "\n")
        QMessageBox.information(self, "Решение", "Файл с результатом называется MultiplyResult.txt\n и находится в"
                                                 "директории с файлом main.py. \nВремя выполнения операции " +
                                str(var2 - var) + " секунд")

    # Задание размеров матриц перед вводом данные для решения СЛАУ
    def slauMatrixNew(self):
        self.answMatrix.clear()
        self.coefMatrix.clear()
        self.answMatrix.setRowCount(self.slauCount.value())
        self.answMatrix.setColumnCount(1)
        self.coefMatrix.setRowCount(self.slauCount.value())
        self.coefMatrix.setColumnCount(self.slauCount.value())

        for i in range(self.coefMatrix.rowCount()):
            for j in range(self.coefMatrix.columnCount()):
                self.coefMatrix.setItem(i, j, QTableWidgetItem(str('')))

        for i in range(self.answMatrix.rowCount()):
            for j in range(self.answMatrix.columnCount()):
                self.answMatrix.setItem(i, j, QTableWidgetItem(str('')))

    # Функция решения систем с помощью библиотеки ТензорФлоу
    def solve(self):
        var = time.time()
        #проверка, чтоб матрица имела столько же строк, сколько и матрица ответов
        if self.coefMatrix.columnCount() == 0 or self.coefMatrix.rowCount() == 0 or self.answMatrix.columnCount() == 0 \
                or self.answMatrix.rowCount() == 0 or self.coefMatrix.rowCount() != self.answMatrix.rowCount():
            QMessageBox.warning(self, "Warning", "Матрицы имеют разное количество строк, либо не заполнены.\n"
                                                 "Решение невозможно!")
            return

        #перегон из листов в нампай
        self.slauCoefMatrix = []
        self.slauAnswMatrix = []

        # Подготовка данных для функции
        for i in range(self.coefMatrix.rowCount()):
            self.slauCoefMatrix.append([])
            for j in range(self.coefMatrix.columnCount()):
                try:
                    float(self.coefMatrix.item(i, j).text())
                    self.slauCoefMatrix[i].append(float(self.coefMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в матрице коэффициентов невозможно представить в виде float.")
                    self.coefMatrix.setCurrentCell(i, j)
                    return
        npCoefSlau = np.asarray(self.slauCoefMatrix, dtype=np.float32)

        for i in range(self.answMatrix.rowCount()):
            self.slauAnswMatrix.append([])
            for j in range(self.answMatrix.columnCount()):
                try:
                    float(self.answMatrix.item(i, j).text())
                    self.slauAnswMatrix[i].append(float(self.answMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в матрице ответов невозможно представить в виде float.")
                    self.answMatrix.setCurrentCell(i, j)
                    return
        npAnswSlau = np.asarray(self.slauAnswMatrix, dtype=np.float32)

        # Работа с Функцией
        try:
            result = tf.linalg.solve(npCoefSlau, npAnswSlau)
            sess = tf.Session()
            text = sess.run(result)
            sess.close()
        except ValueError:
            QMessageBox.warning(self, "Решение", "Не удалось решить СЛАУ.")
            return

        var2 = time.time()

        my_file = open('SolveSLAUResultTensorFlow.txt', 'w')
        my_file.write(str(text))
        QMessageBox.information(self, "Решение", "Файл с результатом называется SolveSLAUResultTensorFlow.txt\n и находится в"
                                                 "директории с файлом main.py. \n Время на выполнение операции" +
                                str(var2 - var) + " секунд.")


    # Заполнение матрицы коэффициентов при решении слау
    def fillCoefMatrix(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "/home", "*.txt")[0]
        self.slauCoefMatrix = []
        if fname != "":
            with open(fname, 'r') as f:
                for line in f:
                    try:
                        self.slauCoefMatrix.append([float(i) for i in line.split()])
                    except ValueError:
                        QMessageBox.warning(self, "Warning", "Поданные на вход данные не соответствуют требованиям\n"
                                                             "Проверьте, возможно в файле использованы скобки, запятые, либо\n"
                                                             " числа переданы неверно. Float можно передавать в форматах -\n"
                                                             "  - через точку. 0.154 0.1222\n"
                                                             "  - через степень. 1e-5 2e10")
                        return
            for i in range(1, len(self.slauCoefMatrix)):
                if len(self.slauCoefMatrix[i]) != len(self.slauCoefMatrix[i - 1]):
                    QMessageBox.warning(self, "Warning", "Матрица имеет строки разной длины.")
                    return
            if len(self.slauCoefMatrix) != len(self.slauCoefMatrix[0]):
                QMessageBox.warning(self, "Warning", "Матрица коэффициентов неквадратная.")
                return
            self.setSlauCoefMatrixData()

    # Заполнение таблицы на экране при выводе из файла
    def setSlauCoefMatrixData(self):
        self.coefMatrix.clear()
        self.slauCount.setValue(len(self.slauCoefMatrix))
        self.coefMatrix.setRowCount(len(self.slauCoefMatrix))
        self.coefMatrix.setColumnCount(len(self.slauCoefMatrix[0]))
        for i in range(len(self.slauCoefMatrix)):
            for j in range(len(self.slauCoefMatrix[i])):
                self.coefMatrix.setItem(i, j, QTableWidgetItem(str(self.slauCoefMatrix[i][j])))

    # Заполнение матрицы ответов при выводе из файла
    def fillAnswMatrix(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "/home", "*.txt")[0]
        self.slauAnswMatrix = []
        if fname != "":
            with open(fname, 'r') as f:
                for line in f:
                    try:
                        self.slauAnswMatrix.append([float(i) for i in line.split()])
                    except ValueError:
                        QMessageBox.warning(self, "Warning", "Поданные на вход данные не соответствуют требованиям\n"
                                                             "Проверьте, возможно в файле использованы скобки, запятые, либо\n"
                                                             " числа переданы неверно. Float можно передавать в форматах -\n"
                                                             "  - через точку. 0.154 0.1222\n"
                                                             "  - через степень. 1e-5 2e10")
                        return
            for i in range(1, len(self.slauAnswMatrix)):
                if len(self.slauAnswMatrix[i]) != 1:
                    QMessageBox.warning(self, "Warning", "Матрица имеет строки разной длиной не равные 1.")
                    return
            self.setSlauAnswMatrixData()

    # Заполнение таблицы на экране при выводе из файла
    def setSlauAnswMatrixData(self):
        self.answMatrix.clear()
        self.answMatrix.setRowCount(len(self.slauAnswMatrix))
        self.answMatrix.setColumnCount(1)
        for i in range(len(self.slauAnswMatrix)):
            self.answMatrix.setItem(i, 0, QTableWidgetItem(str(self.slauAnswMatrix[i][0])))

    # Генерация матрицы заданной размерности
    def generateMatrix(self):

        var = time.time()
        outMat = []
        for i in range(self.genRow.value()):
            outMat.append([])
            strout = ''
            for j in range(self.genColumn.value()):
                digit = np.random.random_sample()
                strout += str(digit)
                strout += " "
            outMat[i].append(strout)

        filename = self.genFilename.text() + ".txt"
        my_file = open(filename, 'w')
        for i in range(len(outMat)):
            my_file.writelines(outMat[i][0] + "\n")
        var2 = time.time()
        QMessageBox.information(self, "Решение", "Файл с результатом находится в директории с проектом. "
                                                 "\n Время на выполнение операции" +
                                str(var2 - var) + " секунд.")


    # Решение слау с помощью МАТЛАБ
    def slauMatLab(self):
        var = time.time()
        # проверка, чтоб матрица имела столько же строк, сколько и матрица ответов
        if self.coefMatrix.columnCount() == 0 or self.coefMatrix.rowCount() == 0 or self.answMatrix.columnCount() == 0 \
                or self.answMatrix.rowCount() == 0 or self.coefMatrix.rowCount() != self.answMatrix.rowCount():
            QMessageBox.warning(self, "Warning", "Матрицы имеют разное количество строк, либо не заполнены.\n"
                                                 "Решение невозможно!")
            return

        # перегон из листов в нампай
        self.slauCoefMatrix = []
        self.slauAnswMatrix = []

        # Подготовка данных для функции
        for i in range(self.coefMatrix.rowCount()):
            self.slauCoefMatrix.append([])
            for j in range(self.coefMatrix.columnCount()):
                try:
                    float(self.coefMatrix.item(i, j).text())
                    self.slauCoefMatrix[i].append(float(self.coefMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в матрице коэффициентов невозможно представить в виде float.")
                    self.coefMatrix.setCurrentCell(i, j)
                    return
        npCoefSlau = np.asarray(self.slauCoefMatrix, dtype=np.float32)

        for i in range(self.answMatrix.rowCount()):
            self.slauAnswMatrix.append([])
            for j in range(self.answMatrix.columnCount()):
                try:
                    float(self.answMatrix.item(i, j).text())
                    self.slauAnswMatrix[i].append(float(self.answMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в матрице коэффициентов невозможно представить в виде float.")
                    self.answMatrix.setCurrentCell(i, j)
                    return
        npAnswSlau = np.asarray(self.slauAnswMatrix, dtype=np.float32)

        # Работа с Функцией
        try:
            result = ml.linalg.solve(npCoefSlau, npAnswSlau)
        except ValueError:
            QMessageBox.warning(self, "Решение", "Не удалось решить СЛАУ.")
            return
        var2 = time.time()
        my_file = open('SolveSLAUResultMatLab.txt', 'w')
        my_file.write(str(result))
        QMessageBox.information(self, "Решение", "Файл с результатом называется SolveSLAUResultMatLab.txt\n и находится"
                                                 "в директории с файлом main.py. \n Время на выполнение операции" +
                                str(var2 - var) + " секунд.")

    # Решение слау через нампай
    def slauNumPy(self):
        var = time.time()
        # проверка, чтоб матрица имела столько же строк, сколько и матрица ответов
        if self.coefMatrix.columnCount() == 0 or self.coefMatrix.rowCount() == 0 or self.answMatrix.columnCount() == 0 \
                or self.answMatrix.rowCount() == 0 or self.coefMatrix.rowCount() != self.answMatrix.rowCount():
            QMessageBox.warning(self, "Warning", "Матрицы имеют разное количество строк, либо не заполнены.\n"
                                                 "Решение невозможно!")
            return

        # перегон из листов в нампай
        self.slauCoefMatrix = []
        self.slauAnswMatrix = []

        # Подготовка данных для функции
        for i in range(self.coefMatrix.rowCount()):
            self.slauCoefMatrix.append([])
            for j in range(self.coefMatrix.columnCount()):
                try:
                    float(self.coefMatrix.item(i, j).text())
                    self.slauCoefMatrix[i].append(float(self.coefMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в матрице коэффициентов невозможно представить в виде float.")
                    self.coefMatrix.setCurrentCell(i, j)
                    return
        npCoefSlau = np.asarray(self.slauCoefMatrix, dtype=np.float32)

        for i in range(self.answMatrix.rowCount()):
            self.slauAnswMatrix.append([])
            for j in range(self.answMatrix.columnCount()):
                try:
                    float(self.answMatrix.item(i, j).text())
                    self.slauAnswMatrix[i].append(float(self.answMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в матрице коэффициентов невозможно представить в виде float.")
                    self.answMatrix.setCurrentCell(i, j)
                    return
        npAnswSlau = np.asarray(self.slauAnswMatrix, dtype=np.float32)

        # Работа с Функцией
        try:
            result = np.linalg.solve(npCoefSlau, npAnswSlau)
        except ValueError:
            QMessageBox.warning(self, "Решение", "Не удалось решить СЛАУ.")
            return
        var2 = time.time()
        my_file = open('SolveSLAUResultNumPy.txt', 'w')
        my_file.write(str(result))
        QMessageBox.information(self, "Решение", "Файл с результатом называется SolveSLAUResultNumPy.txt\n и находится в"
                                                 "директории с файлом main.py. \n Время на выполнение операции" +
                                str(var2 - var) + " секунд.")

    # Умножение матриц с помощью нампай
    def multiplyNumpy(self):
        var = time.time()
        # Проверка, чтоб в матрицах хоть что-то было
        if self.leftMatrix.columnCount() == 0 or self.leftMatrix.rowCount() == 0 or self.rightMatrix.columnCount() == 0 \
                or self.rightMatrix.rowCount() == 0 or self.leftMatrix.columnCount() != self.rightMatrix.rowCount():
            QMessageBox.warning(self, "Warning", "Количесво строк и столбцов в матрицах не позволяет их перемножить")
            return

        self.mulLeftMatrix = []
        self.mulRightMatrix = []

        # Подготовка данных для функции
        for i in range(self.leftMatrix.rowCount()):
            self.mulLeftMatrix.append([])
            for j in range(self.leftMatrix.columnCount()):
                try:
                    float(self.leftMatrix.item(i, j).text())
                    self.mulLeftMatrix[i].append(float(self.leftMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в левой матрице невозможно представить в виде float.")
                    self.leftMatrix.setCurrentCell(i, j)
                    return
        npLeftMul = np.asarray(self.mulLeftMatrix, dtype=np.float32)

        for i in range(self.rightMatrix.rowCount()):
            self.mulRightMatrix.append([])
            for j in range(self.rightMatrix.columnCount()):
                try:
                    float(self.rightMatrix.item(i, j).text())
                    self.mulRightMatrix[i].append(float(self.rightMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в правой матрице невозможно представить в виде float.")
                    self.rightMatrix.setCurrentCell(i, j)
                    return
        npRightMul = np.asarray(self.mulRightMatrix, dtype=np.float32)

        # Работа с Функцией
        try:
            result = np.matmul(npLeftMul, npRightMul)
        except ValueError:
            QMessageBox.warning(self, "Решение", "Не удалось перемножить матрицы.")
            return
        var2 = time.time()
        my_file = open('MultiplyResultNumPy.txt', 'w')
        for i in range(len(result)):
            my_file.write(str(result[i]) + "\n")
        QMessageBox.information(self, "Решение", "Файл с результатом называется MultiplyResultNumPy.txt\n и находится в"
                                                 "директории с файлом main.py. \n Время на выполнение операции" +
                                str(var2 - var) + " секунд.")

    # Умножение матриц с помощью матлаб
    def multiplyMatLab(self):
        var = time.time()
        # Проверка, чтоб в матрицах хоть что-то было
        if self.leftMatrix.columnCount() == 0 or self.leftMatrix.rowCount() == 0 or self.rightMatrix.columnCount() == 0 \
                or self.rightMatrix.rowCount() == 0 or self.leftMatrix.columnCount() != self.rightMatrix.rowCount():
            QMessageBox.warning(self, "Warning", "Количесво строк и столбцов в матрицах не позволяет их перемножить")
            return

        self.mulLeftMatrix = []
        self.mulRightMatrix = []

        # Подготовка данных для функции
        for i in range(self.leftMatrix.rowCount()):
            self.mulLeftMatrix.append([])
            for j in range(self.leftMatrix.columnCount()):
                try:
                    float(self.leftMatrix.item(i, j).text())
                    self.mulLeftMatrix[i].append(float(self.leftMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в левой матрице невозможно представить в виде float.")
                    self.leftMatrix.setCurrentCell(i, j)
                    return
        npLeftMul = np.asarray(self.mulLeftMatrix, dtype=np.float32)

        for i in range(self.rightMatrix.rowCount()):
            self.mulRightMatrix.append([])
            for j in range(self.rightMatrix.columnCount()):
                try:
                    float(self.rightMatrix.item(i, j).text())
                    self.mulRightMatrix[i].append(float(self.rightMatrix.item(i, j).text()))
                except ValueError:
                    QMessageBox.warning(self, "Warning",
                                        "Значение в правой матрице невозможно представить в виде float.")
                    self.rightMatrix.setCurrentCell(i, j)
                    return
        npRightMul = np.asarray(self.mulRightMatrix, dtype=np.float32)

        # Работа с Функцией
        try:
            result = ml.matmul(npLeftMul, npRightMul)
        except ValueError:
            QMessageBox.warning(self, "Решение", "Не удалось перемножить матрицы.")
            return

        var2 = time.time()
        my_file = open('MultiplyResultMatlab.txt', 'w')
        for i in range(len(result)):
            my_file.write(str(result[i]) + "\n")
        QMessageBox.information(self, "Решение", "Файл с результатом называется MultiplyResultMatlab.txt\n и находится в"
                                                 "директории с файлом main.py. \n Время на выполнение операции" +
                                str(var2 - var) + " секунд.")



# Основная функция, которая будет запускать приложение
def main():
    app = QtWidgets.QApplication(sys.argv)
    # Указзываем, окно какого класса необходимо создать
    window = DiplomApp()
    window.show()
    app.exec_()

# Если запуск файла производится напрямую, а не через импорт
if __name__ == '__main__':
    # то необходимо запустить мэйн
    main()
